**DISCLAIMER: this is for test purposes only**



First, create the base container used for other steps:

```
docker build -f Dockerfile.base base
```

Then, build your version of Carla, based on the previous container images:

```
docker build -f Dockerfile.carla carla
```


If you want to push to some container registry:

```
docker tag carla MY_REGISTY/carla/carla
docker push MY_REGISTY/carla/carla
```



Let me know if you have questions.